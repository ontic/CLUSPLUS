"""clus2.py"""

import pyspark
from sys import argv 
from time import time
import numpy as np
from sklearn.cluster import DBSCAN
import itertools
import scipy as sp
import bisect

#----------------------------------------------
#		FUNCTIONS
#----------------------------------------------

# Input: partition of RDD string s (a NWpacket with all the features)
# Output: (key,[val]) -> (intRow,floatFeaturesPerRow[])
def mTorv (s):
	a=s.split(" ")
	b=[]
	for i in range(len(a)-1):
		b.append(float(a[i+1]))
	return (int(a[0]),b)

# Input: (intRow,[floatFeaturesPerRow]) 
# Output: array of (intCol,([intRow],[floatVal]))
def fmTocrv (tupl):
	tuparray=[]
	for i in range(len(tupl[1])): # for each col number
		tuparray.append((i,([tupl[0]],[tupl[1][i]])))
	return tuparray

# Input: ([intRow],[floatVal]),([intRow],[floatVal])
# Output: ([intRow],[floatVal])
def rbkGroupCols(tupl1, tupl2):
	t1 = []
	t2 = []
	t1.extend(tupl1[0])
	t1.extend(tupl2[0])
	t2.extend(tupl1[1])
        t2.extend(tupl2[1])
        return (t1,t2)

# Input: (intROWS[], floatVALS[])
# Output: (UpdatedROWS[],clusteredRealVALS[],res)
def mvDodbs (tupl):
        n = np.matrix(tupl[1])
        result = DBSCAN(eps.value,minP.value).fit_predict(n.getT())
        cont = 0
        rem =[]
        for i in range(len(result)):
                if result[i] == -1:
                        rem.append(i)
                        del tupl[0][i-cont]
                        del tupl[1][i-cont]
                        cont = cont + 1
        res = np.delete(result,rem)
        if len(res) !=0:
                return (tupl[0],tupl[1],res)

# Input: (col)
# Output: [[col,col][col,col]...] or None
def mAddPosib1(col):
	t = []
	if col != keys.value[-1]:
		for i in keys.value:
			if col < i:
				t.append([col,i])
		return t

def mSortValues( ((rows1,vals1),(rows2,vals2)) ):
        i = 0
        j = 0
	rows = []
	vals = []
	#print(rows1)
	#print(rows2)
	#print(vals1)
	#print(vals2)
        while (i < len(rows1) and j < len(rows2)):
                if (rows1[i] > rows2[j]):
			j = j+1
                elif (rows2[j] > rows1[i]):
			i = i+1
                else:
			#print(i)
			rows.append(rows1[i])
			v = []
			for t in range(len(vals1)):
				v.append(vals1[t][i])
			v.append(vals2[j])
			vals.append(v)
			i = i+1
                        j = j+1
	#print(rows)
	if len(rows) >= minP.value:
		#lol = np.asarray(vals).T.tolist()
        	return (rows,vals)

def mvDodbs2( (rows,vals) ):
        n = np.matrix(vals)
	m = n.getT().tolist()
	result = DBSCAN(eps.value,minP.value).fit_predict(n)
	res = []
	cont = 0
	for i in range(len(result)):
                if result[i] == -1:
			del rows[i-cont]
			for k in m:
				del k[i-cont]
			cont = cont+1
		else:
			res.append(result[i])
        if len(res) != 0:
		return (rows,m,res)

# Input: (cols)
# Output: [[cols][cols]...] or None
def mAddPosib2(cols):
        res = []
	diff = np.setdiff1d(np.array(keys.value),np.array(cols))
	for i in diff:
		aux = []
		aux.extend(cols)
		aux.append(i)
		res.append(sorted(aux))
	return res

def mSplitAndRemLast((cols,cont)):
	last = cols.pop()
	return ((str(cols),(cols,last)))

def mJoinCols((c2,((cols1,rv1),rv2))):
	cols = []
	cols.extend(cols1)
	cols.append(c2)
	return (cols,(rv1,rv2))

#----------------------------------------------
#               Variables
#----------------------------------------------
if len(argv)!=6:
	print("ERROR: Use -> rddTestPython.py <InputFilePath> <OutputFilePath> <NumPartitions> <epsilon> <minPoints>")
	exit(0)

inputFile = argv[1]
outputFile = argv[2]
numPart = int(argv[3])
epsilon = float(argv[4])
minPoints = int(argv[5])

#----------------------------------------------
#		Main
#----------------------------------------------
start = time()

sc=pyspark.SparkContext()
textFile = sc.textFile(inputFile,numPart).map(mTorv,True)

eps = sc.broadcast(epsilon)#0.2
minP = sc.broadcast(minPoints)#8

# Dim = 1
db = textFile.flatMap(fmTocrv,True).reduceByKey(rbkGroupCols,numPart).mapValues(mvDodbs).filter(lambda (x,y): y is not None).persist()
data = db.mapValues(lambda (r,v,res): (len(r),r,res)).persist(storageLevel=pyspark.StorageLevel(True, False, False, False, 1))  #Final Result stores only col rows[] clusterID

k = db.keys().collect()
maxDim = len(k)
keys = sc.broadcast(sorted(k))

# At least 2 combinable subspaces for Dim2
if (maxDim > 1):
	dbs1 = db.mapValues(lambda (r,v,res): (r,v)).mapValues(lambda (r,v): zip(*sorted(zip(r,v)))).persist()
	db.unpersist()
	print("dbs1: ")
	loop = 0
        stop = False

        while ((loop < maxDim-1) and (not stop)):
		print("Dim +1")
                loop = loop+1
		# Two dimensions
		if loop == 1:
			candidates = dbs1.keys().map(mAddPosib1,True).filter(lambda x: x is not None).flatMap(lambda cols: cols,True).persist()
			numCand = candidates.count()
			print("Num candidates: ", numCand)
			if (numCand != 0):
				aux = candidates.map(lambda cols: (cols[0],cols[1]),True).leftOuterJoin(dbs1).repartition(numPart).map(lambda (c0,(c1,rv)): (c1,(c0,rv)),True).leftOuterJoin(dbs1,numPart)
				candidates.unpersist()
				aux2 = aux.map(lambda (c2,((c1,(r1,v1)),(r2,v2))): ([c1,c2],((r1,[v1]),(r2,v2))),True).mapValues(mSortValues).filter(lambda (x,y): y is not None)
				aux3 = aux2.mapValues(mvDodbs2).filter(lambda (x,y): y is not None).persist()
				data = sc.union([data,aux3.mapValues(lambda (r,v,res): (len(r),r,res))])
				dbs = aux3.mapValues(lambda (r,v,res): (r,v)).persist()
                                if(dbs.count() != 0):
                                    finaldata = dbs
				aux3.unpersist()
				print("dbs: ")
			else:
                        	stop = True					
		# More then two dimensions
		else:
			candidates = dbs.keys().map(mAddPosib2,True).flatMap(lambda cols: cols,True).map(lambda cols: (str(cols),(cols,1)),True).reduceByKey(lambda (cols1,cont1),(cols2,cont2): (cols1,cont1+cont2),numPart).filter(lambda (id,(cols,cont)): cont >= (loop+1)).values().persist()
			numCand = candidates.count()
                	print("Num candidates: ", numCand)
			if (numCand != 0):
				db = dbs.map(lambda (c,(r,v)): (str(c),(r,v)),True)
				dbs.unpersist()
				aux = candidates.map(mSplitAndRemLast,True).leftOuterJoin(db,numPart).values().map(lambda ((cols,c),rv): (c,(cols,rv)),True).leftOuterJoin(dbs1,numPart)
				candidates.unpersist()
				aux2 = aux.map(mJoinCols,True).mapValues(mSortValues).filter(lambda (x,y): y is not None)
				aux3 = aux2.mapValues(mvDodbs2).filter(lambda (x,y): y is not None).persist()
				dbs = aux3.mapValues(lambda (r,v,res): (r,v)).persist()
                                if(dbs.count() != 0):
                                    finaldata = dbs
				data = sc.union([data,aux3.mapValues(lambda (r,v,res): (len(r),r,res))])
				aux3.unpersist()
				print("dbs2: ")
			else:
				stop = True
                print(loop)
data.saveAsTextFile(outputFile)
sc.stop()
end = time()-start
print ("Processed in: " + str(end))
