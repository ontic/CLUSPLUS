#  CLUSPLUS                   
## Description
 CLUSPLUS is an improved version of CLUS subspace clustering algorithm (see the publication section) by using optimized primitives of Spark that contributes to a significant improvement of performance. 
Clustering on high-dimensional datasets in many investigation fields like network traffic characterization is becoming more and more challenging. This is because meaningful clusters can hide in different subspaces of the full feature space which usually consists of hundreds of features. Subspace clustering algorithms try to find all low-dimensional clusters hidden in subspaces of high dimensional data. 
 CLUSPLUS is a novel parallel subspace clustering algorithm on top of the Spark platform based on SUBCLU algorithm. Traditional centralized subspace clustering techniques intended to find hidden clusters that exist in certain subsets of the full feature spaces. These centralized algorithms assume that the whole dataset can fit in the RAM of a single machine. However, as current datasets in application domains, especially in the context of network traffic characterization, become larger this assumption no longer holds. Moreover, existing non-approximate subspace clustering algorithms have at least exponential time complexity. For example, for a relatively medium-sized dataset, SUBCLU algorithm took six days to terminate.
 CLUSPLUS uses a new dynamic data partitioning method specifically designed to continuously optimize the varying size and content of required data for each iteration in order to fully take advantage of Spark’s in-memory primitives. This method minimizes communication cost between nodes, maximizes their CPU usage, and balances the load among them. In summary, by means of parallelism the execution time of  CLUSPLUS is significantly reduced comparing to other alternatives. 
### Software architecture
 CLUSPLUS is implemented on top of Spark using its specific primitives. Specifically, it has been designed and tested on Apache Hadoop.
 CLUSPLUS intends to detect all density-based clusters in all subspaces following a bottom-up, greedy strategy. A clustering process is first performed over each dimension on each core to generate the set of 1-D subspaces containing clusters. Secondly,  CLUSPLUS generates a set of candidates for different dimensionality in an iterative way. The Anti-Monotonicity property and a density-based pruning technique are used to prune redundant candidates. The best (K-1)-D subset of K-D candidate subspace with minimum shuffle cost is carefully chosen as the to reduce the communication between nodes. The iterative process terminates when no more clusters are detected. 
Comparing to CLUS (see the publication section), the improved algorithm is redesigned to reduce read and write operations of the original input dataset. Candidate generation method is also improved to consider less but more appropriate candidates.  CLUSPLUS has the following differences: 1) the <key, value> structure is redesigned to increase the level of parallelism 2) it improves the approach of candidate generation by preventing considering those unappropriated candidates, thus greatly reduces the number of tasks for higher dimensional iterations. 3) The data flow of different iterations is deeply inspected and controlled to guarantee that only necessary data are shuffled between nodes. 4) the visualization of clustering results is improved to avoid cluster redundancy.
### Parameters
  CLUSPLUS requires the following parameters:

* Input filename: name of the input file that contains the input data.

* Output filename: name of the output file where the subspace clustering results will be stored.

* Number of partitions: the number of partitions of the RDD that contains the input data. The higher this parameter is, the higher the level of parallelism is.

* Epsilon: The maximum distance between two data objects for them to be considered as in the same neighborhood.

* minPoints: The number of data objects in a neighborhood for a object to be considered as a core object, including the object itself.

## How To
### Platform
  CLUSPLUS is implemented using Python 2.7.6 and tested on a commodity cluster of 10 nodes. Each node of the cluster consists of Core(TM) 2 Quad CPUs with 4GB of RAM, and runs CentOS operating system, Spark version 1.2.2 and Hadoop 2.6.0.
### Input file format
The expected input file is an mxn numeric matrix. Each of the m lines represents an data object of n dimensions, and consists of the object index and a string of n whitespace-separated numbers. For instance:

```0 19.59981 28.21584 20.76166 16.59425 1.6909167774 ...```

```1 19.22174 23.98512 19.7585 27.51049 80.6015194996 ...```

```2 25.07013 20.21528 18.4996 24.0048 55.1228937646 ...```

### Run the program
The project runs using PySpark. Three libraries are needed to successfully run the program: numpy, scipy and sklearn.cluster

Before running   CLUSPLUS, the textual file containing the input dataset must be copied in the distributed file system (HDFS), e.g.,

hadoop dfs -put example.txt

The command to run the algorithm is as follows:

spark-submit   CLUSPLUS.py `<InputFilePath> <OutputFilePath> <NumPartitions> <epsilon> <minPoints>` e.g.,

spark-submit   CLUSPLUS.py /example.txt outputFile 4 4.0 30
### Results and output file format
The results (i.e., the set of subspace clusters) can be found in the HDFS directory `<output_directory>/`. Each paragraph of the output file contains one subspace cluster. Each subspace cluster is correlated with its subspace, size, and set of data object indexes. Each paragraph in the output file is in the form:

(subspace[dimension1,dimension2,...], (sizeOfClusters, setOfDataObjects[object1,object2,...], clusterLabelsForObjects[label1,label2,...])) e.g.,

([0, 1, 2], (893, [0, 1, 2, 3, 4, 5,..., 721, 722, 723, 724, 725, 726, ...], [0, 0, 0, 0, 0, 0,..., 1, 1, 1, 1, 1, 1,...,])) represents a subspace [0, 1, 2] that contains 893 clustered data objects that belong to 2 different clusters.
## Examples    
An example data set called `example.txt` is available in folder data. Below is an execution example with epsilon = 4.0 and minPoints = 30.

spark-submit   CLUSPLUS.py /example.txt outputFile 4 4.0 30

The subspace clustering results can be retrieved by viewing text files in the outputFile folder:

cat outputFile/part*
## Publications 
The original CLUS algorithm has been presented in the following paper:
Zhu Bo, Alexandru Mara, and Alberto Mozo. "CLUS: Parallel Subspace Clustering Algorithm on Spark." New Trends in Databases and Information Systems. Springer International Publishing, 2015. 175-185. DOI: http://link.springer.com/chapter/10.1007/978-3-319-23201-0_20

The original SUBCLU algorithm was presented in the following paper:
Kailing Karin, Hans-Peter Kriegel, and Peer Kröger. "Density-connected subspace clustering for high-dimensional data." 2004 SIAM International Conference on DATA MINING (SDM2004). April 22-24, 2004, Lake Buena Vista, Florida, pp. 111-118, DOI: http://epubs.siam.org/doi/pdf/10.1137/1.9781611972740.23

 CLUSPLUS will be included in a journal paper which is under submission.

